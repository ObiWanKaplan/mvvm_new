package ru.psu.mobileapp.model

import androidx.room.Entity

@Entity(
    tableName                               = "statuses"
)
class CStatus                               : CNamedObject()