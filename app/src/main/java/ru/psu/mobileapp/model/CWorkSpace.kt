package ru.psu.mobileapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.squareup.moshi.Json

@Entity(
    tableName                               = "workspaces"
)
class CWorkSpace(
    @Json(name                              = "nextSequenceNumber")
    @ColumnInfo(name                        = "nextSequenceNumber")
    var nextSequenceNumber                  : Int?
                                            = null
)                                           : CNamedObject()