package ru.psu.mobileapp.model

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import java.util.*
import kotlin.reflect.full.isSubclassOf

/********************************************************************************************************
 * Базовый класс для всех объектов модели.                                                              *
 * @author Селетков И.П. 2018 0816.                                                                     *
 *******************************************************************************************************/
abstract class CObjectBase
(
    /****************************************************************************************************
     * Идентификатор.                                                                                   *
     ***************************************************************************************************/
    @PrimaryKey(autoGenerate                = false)
    @Json(name                              = "id")
    @ColumnInfo(name                        = "id")
    var id                                  : UUID
                                            = UUID.randomUUID()
)
{
    override fun equals(other: Any?)        : Boolean
    {
        other ?: return false
        if (other is CObjectBase || other::class.isSubclassOf(CObjectBase::class))
            return id == (other as CObjectBase).id

        return false
    }

    override fun hashCode()                 : Int {
        return id.hashCode()
    }
}