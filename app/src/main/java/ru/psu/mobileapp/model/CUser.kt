package ru.psu.mobileapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.squareup.moshi.Json
import org.threeten.bp.LocalDateTime
import java.util.*


@Entity(
    tableName                               = "users"
)
class CUser     (

    /****************************************************************************************************
     * Идентификатор из Окты.                                                                           *
     ***************************************************************************************************/
    @Json(name                              = "user")
    @ColumnInfo(name                        = "user_id")
    var userId                              : String?
                                            = null,

//    @Json(name                              = "name")
//    @ColumnInfo(name                        = "name")
//    var name                                : String?
//                                            = null,

    @Json(name                              = "firstName")
    @ColumnInfo(name                        = "firstName")
    var firstName                           : String?
                                            = null,

    @Json(name                              = "middleName")
    @ColumnInfo(name                        = "middleName")
    var middleName                          : String?
                                            = null,

    @Json(name                              = "lastName")
    @ColumnInfo(name                        = "lastName")
    var lastName                            : String?
                                            = null,

    @Json(name                              = "organization")
    @ColumnInfo(name                        = "organization_id")
    var organizationId                      : UUID?
                                            = null,

    @Json(name                              = "position")
    @ColumnInfo(name                        = "position")
    var position                            : String?
                                            = null,

    @Json(name                              = "mobilePhone")
    @ColumnInfo(name                        = "mobilePhone")
    var mobilePhone                         : String?
                                            = null,

    @Json(name                              = "email")
    @ColumnInfo(name                        = "email")
    var email                               : String?
                                            = null
)                                           : CNamedObject()
{
    fun Name()                              : String
    {
        if ((firstName != null) && (middleName != null) && (lastName != null))
        return firstName + " " + middleName + " " + lastName
        else
        {
            if ((firstName != null) && (middleName != null))
                return firstName + " " + middleName
            else
            {
                if ((firstName != null) && (lastName != null))
                    return firstName + " " + lastName
                else
                {
                    if (firstName != null)
                        return firstName + ""
                    else if (middleName != null)
                        return middleName +""
                    else if (lastName != null)
                        return lastName + ""

                }
            }
        }
        return "Не назначен"
    }
}