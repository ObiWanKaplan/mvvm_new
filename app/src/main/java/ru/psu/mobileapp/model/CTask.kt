package ru.psu.mobileapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import com.squareup.moshi.Json
import org.threeten.bp.LocalDateTime
import java.util.*

@Entity(
    tableName                               = "tasks",
    indices                                 = [
        Index("status_id")
    ],
    foreignKeys                             = [
        ForeignKey(
            entity                          = CStatus::class,
            parentColumns                   = ["id"],
            childColumns                    = ["status_id"]
        )
    ]

)
class CTask
    (

    /****************************************************************************************************
     * Наименование.                                                                                    *
     ***************************************************************************************************/
    @Json(name                              = "deadline")
    @ColumnInfo(name                        = "deadline")
    var deadline                            : LocalDateTime?
                                            = null,

    @Json(name                              = "status")
    @ColumnInfo(name                        = "status_id")
    var statusId                            : UUID?
                                            = null,

    @Json(name                              = "object")
    @ColumnInfo(name                        = "object_id")
    var objectId                            : UUID?
                                            = null,

    @Json(name                              = "creator")
    @ColumnInfo(name                        = "creator_id")
    var creatorId                           : UUID?
                                            = null,

    @Json(name                              = "initiator")
    @ColumnInfo(name                        = "initiator_id")
    var initiatorId                         : UUID?
                                            = null,

    @Json(name                              = "assignee")
    @ColumnInfo(name                        = "assignee_id")
    var assigneeId                          : UUID?
                                            = null,

    @Json(name                              = "acceptanceDate")
    @ColumnInfo(name                        = "acceptanceDate")
    var acceptanceDate                      : LocalDateTime?
                                            = null,

    @Json(name                              = "assignmentDate")
    @ColumnInfo(name                        = "assignmentDate")
    var assignmentDate                      : LocalDateTime?
                                            = null,

    @Json(name                              = "endDate")
    @ColumnInfo(name                        = "endDate")
    var endDate                             : LocalDateTime?
                                            = null,

    @Json(name                              = "sequenceNumber")
    @ColumnInfo(name                        = "sequenceNumber")
    var sequenceNumber                      : Int?
                                            = null
)                                           : CNamedObject()