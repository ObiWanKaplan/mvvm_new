package ru.psu.mobileapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.squareup.moshi.Json
import java.util.*

@Entity(
    tableName                               = "objects"
)

class CObject (

    @Json(name                              = "organization")
    @ColumnInfo(name                        = "organization_id")
    var organizationId                        : UUID?
                                            = null,

    @Json(name                              = "workspace")
    @ColumnInfo(name                        = "workspace_id")
    var workSpaceId                         : UUID?
                                            = null

)                                           : CNamedObject()
