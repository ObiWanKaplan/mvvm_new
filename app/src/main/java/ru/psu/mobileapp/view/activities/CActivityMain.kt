package ru.psu.mobileapp.view.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import me.tatarka.bindingcollectionadapter2.ItemBinding
import ru.psu.mobileapp.BR
import ru.psu.mobileapp.R
import ru.psu.mobileapp.databinding.ActivityMainBinding
import ru.psu.mobileapp.model.CWorkSpace
import javax.inject.Inject
import ru.psu.mobileapp.viewmodel.CViewModelActivityMain
import ru.psu.mobileapp.viewmodel.IClickListener

class CActivityMain                         : CActivityBase(), IClickListener
{
    @Inject
    lateinit var viewModelFactory           : ViewModelProvider.Factory
    private lateinit var viewModel          : CViewModelActivityMain


    lateinit var singleWorkspaceBinding     : ItemBinding<CWorkSpace>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel                           = viewModel(viewModelFactory){}
        bind()
    }



    private fun bind(): View {
        val binding                         : ActivityMainBinding
                                            = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel                   = viewModel
        binding.view                        = this
        binding.lifecycleOwner = this

        singleWorkspaceBinding              = ItemBinding.of<CWorkSpace>(BR.workspace, R.layout.recycleview_workspace_item)
            .bindExtra(BR.listener, this)

        return binding.root
    }


    override fun onItemClick(item: Any) {
        if (item !is CWorkSpace)
            return

        val intent = Intent(this, CActivityTasks::class.java)
        intent.putExtra("WorkSpaceId", item.id)
        startActivity(intent)
    }
}

