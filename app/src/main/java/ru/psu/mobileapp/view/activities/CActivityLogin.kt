package ru.psu.mobileapp.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.okta.appauth.android.OktaAppAuth
import net.openid.appauth.AuthorizationException
import android.app.PendingIntent
import android.content.Intent
import io.reactivex.android.schedulers.AndroidSchedulers
import ru.psu.mobileapp.R


class CActivityLogin                        : AppCompatActivity()
{
    private var mOktaAuth                   : OktaAppAuth?
                                            = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mOktaAuth                           = OktaAppAuth.getInstance(this)
        // Если пользователь уже есть
        if (mOktaAuth!!.isUserLoggedIn)
        {
            startActivity(Intent(this, CActivityMain::class.java))
            finish()
            return
        }

        // Do any of your own setup of the Activity
        mOktaAuth!!.init(
            this,
            object : OktaAppAuth.OktaAuthListener {
                override fun onSuccess()
                {
                    // Handle a successful initialization (e.g. display login button)
                //    Toast.makeText(this@CActivityLogin, "Подключение к Okta настроено.", Toast.LENGTH_SHORT).show()
                    authenticate()
                }

                override fun onTokenFailure(ex: AuthorizationException)
                {
                    // Handle a failed initialization
                    Toast.makeText(this@CActivityLogin, "Подключение к Okta не удалось настроить. ${ex.errorDescription}", Toast.LENGTH_SHORT).show()
                }
            })
    }
    fun authenticate(
    )
    {
        val completionIntent                = Intent(this, CActivityMain::class.java)
        val cancelIntent = Intent(this, CActivityLogin::class.java)
        cancelIntent.flags                  = Intent.FLAG_ACTIVITY_CLEAR_TOP

        mOktaAuth!!.login(
            this,
            PendingIntent.getActivity(this, 0, completionIntent, 0),
            PendingIntent.getActivity(this, 0, cancelIntent, 0)
        )
    }
}
