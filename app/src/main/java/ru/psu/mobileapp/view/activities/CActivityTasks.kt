package ru.psu.mobileapp.view.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import ru.psu.mobileapp.R

import me.tatarka.bindingcollectionadapter2.ItemBinding
import ru.psu.mobileapp.BR
import ru.psu.mobileapp.databinding.ActivityTasksBinding
import ru.psu.mobileapp.model.CTask
import ru.psu.mobileapp.viewmodel.CViewModelActivityTaskDetails
import ru.psu.mobileapp.viewmodel.CViewModelActivityTasks
import ru.psu.mobileapp.viewmodel.IClickListener
import java.util.*
import javax.inject.Inject

class CActivityTasks                        : CActivityBase(), IClickListener
{
    @Inject
    lateinit var viewModelFactory           : ViewModelProvider.Factory
    private lateinit var viewModelTask          : CViewModelActivityTasks

    lateinit var singleTaskBinding          : ItemBinding<CTask>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelTask                       = viewModel(viewModelFactory){}
        val workspaceId              = intent.getSerializableExtra("WorkSpaceId") as UUID
        viewModelTask.workspaceId           = workspaceId

        bind()
    }

    private fun bind(): View {

        val binding                         : ActivityTasksBinding
                                            = DataBindingUtil.setContentView(this, R.layout.activity_tasks)
        binding.viewModel                   = viewModelTask
        binding.view                        = this
        binding.lifecycleOwner = this

        singleTaskBinding                   = ItemBinding.of<CTask>(BR.task, R.layout.recycleview_task_item)
            .bindExtra(BR.viewModel, viewModelTask)
            .bindExtra(BR.listener, this)

        return binding.root
    }


    override fun onItemClick(item: Any) {
        if (item !is CTask)
            return

        val intent = Intent(this, CActivityTaskDetails::class.java)
        intent.putExtra("TaskId", item.id)
        startActivity(intent)
    }

}