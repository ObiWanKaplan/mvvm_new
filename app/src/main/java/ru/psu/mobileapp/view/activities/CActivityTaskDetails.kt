package ru.psu.mobileapp.view.activities

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import me.tatarka.bindingcollectionadapter2.ItemBinding
import ru.psu.mobileapp.BR
import ru.psu.mobileapp.databinding.ActivityTaskDetailsBinding
import ru.psu.mobileapp.viewmodel.CViewModelActivityTaskDetails
import java.util.*
import javax.inject.Inject
import ru.psu.mobileapp.R
import ru.psu.mobileapp.model.CObject
import ru.psu.mobileapp.model.CStatus
import ru.psu.mobileapp.model.CTask
import ru.psu.mobileapp.model.CUser


class CActivityTaskDetails                  : CActivityBase()
{
    @Inject
    lateinit var viewModelFactory       : ViewModelProvider.Factory
    private lateinit var viewModelTaskDetails   : CViewModelActivityTaskDetails

    lateinit var singleUserBinding          : ItemBinding<CUser>
    lateinit var singleStatusBinding        : ItemBinding<CStatus>
    lateinit var singleTaskBinding          : ItemBinding<CTask>
    lateinit var singleObjectBinding        : ItemBinding<CObject>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelTaskDetails            = viewModel(viewModelFactory){}
        val taskId               = intent.getSerializableExtra("TaskId") as UUID
        viewModelTaskDetails.taskId     = taskId

        bind()
    }

    private fun bind(): View {

        val binding                         : ActivityTaskDetailsBinding
                                            = DataBindingUtil.setContentView(this, R.layout.activity_task_details)
        binding.viewModelTaskDetails        = viewModelTaskDetails
        binding.lifecycleOwner              = this
        binding.view                        = this

        singleUserBinding                   = ItemBinding.of<CUser>(BR.user, R.layout.spinner_user_item)
        singleStatusBinding                 = ItemBinding.of<CStatus>(BR.status, R.layout.spinner_status_item)
        singleObjectBinding                 = ItemBinding.of<CObject>(BR.objects, R.layout.spinner_object_item)



        return binding.root
    }

    fun showAssignmentDate()
    {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener { view, year, month, day ->
                viewModelTaskDetails.updateAssignmentDate(year, month, day)
            }, year, month, day
        )
        dpd.show()
    }

    fun showDeadline()
    {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener { view, year, month, day ->
                viewModelTaskDetails.updateDeadline(year, month, day)
            }, year, month, day
        )
        dpd.show()
    }
}