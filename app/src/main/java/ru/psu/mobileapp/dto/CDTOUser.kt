package ru.psu.mobileapp.dto

import android.provider.ContactsContract
import androidx.room.ColumnInfo
import com.squareup.moshi.Json
import ru.psu.mobileapp.model.CNamedObject
import ru.psu.mobileapp.model.COrganization
import ru.psu.mobileapp.model.CUser
import ru.psu.mobileapp.model.CWorkSpace
import java.util.*

class CDTOUser (

    /****************************************************************************************************
     * Идентификатор из Окты.                                                                           *
     ***************************************************************************************************/
    @Json(name                              = "userId")
    var userId                              : String?
                                            = null,

    @Json(name                              = "firstName")
    var firstName                           : String?
                                            = null,

    @Json(name                              = "middleName")
    var middleName                          : String?
                                            = null,

    @Json(name                              = "lastName")
    var lastName                            : String?
                                            = null,

    @Json(name                              = "organization")
    var organization                        : COrganization?
                                            = null,

    @Json(name                              = "position")
    var position                            : String?
                                            = null,

    @Json(name                              = "mobilePhone")
    var mobilePhone                         : String?
                                            = null,

    @Json(name                              = "email")
    var email                               : String?
                                            = null

)                                           : CNamedObject()
{
    fun toUser()                            : CUser
    {
        val user                            = CUser(
            userId,
            firstName,
            middleName,
            lastName,
            organization?.id,
            position,
            mobilePhone,
            email
        )
        user.id                             = id
        user.createdAt                      = createdAt
        user.updatedAt                      = updatedAt
        user.active                         = active
        user.name                           = name
        user.description                    = description
        return user
    }


}