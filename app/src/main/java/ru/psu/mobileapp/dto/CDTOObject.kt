package ru.psu.mobileapp.dto

import androidx.room.ColumnInfo
import com.squareup.moshi.Json
import ru.psu.mobileapp.model.*

class CDTOObject (

    @Json(name                              = "organization")
    @ColumnInfo(name                        = "organization")
    var organization                        : COrganization?
                                            = null,

    @Json(name                              = "workspace")
    @ColumnInfo(name                        = "workspace")
    var workspace                           : CWorkSpace?
                                            = null

)                                           : CNamedObject()
{
    fun toObject()                          : CObject
    {
        val `object`                        = CObject(organization?.id, workspace?.id)
        `object`.id                         = id
        `object`.name                       = name
        `object`.description                = description
        `object`.createdAt                  = createdAt
        `object`.updatedAt                  = updatedAt
        `object`.active                     = active
        return `object`
    }
}