package ru.psu.mobileapp.dto

import androidx.room.ColumnInfo
import com.squareup.moshi.Json
import org.threeten.bp.LocalDateTime
import ru.psu.mobileapp.model.CNamedObject
import ru.psu.mobileapp.model.CStatus
import ru.psu.mobileapp.model.CTask
import ru.psu.mobileapp.model.CWorkSpace

class CDTOTask
        (

        /****************************************************************************************************
         * Наименование.                                                                                    *
         ***************************************************************************************************/
        @Json(name                              = "deadline")
        @ColumnInfo(name                        = "deadline")
        var deadline                            : LocalDateTime?
                                                = null,

        @Json(name                              = "status")
        @ColumnInfo(name                        = "status_id")
        var status                              : CStatus?
                                                = null,

        @Json(name                              = "creator")
        @ColumnInfo(name                        = "creator")
        var creator                             : CDTOUser?
                                                = null,

        @Json(name                              = "initiator")
        @ColumnInfo(name                        = "initiator")
        var initiator                           : CDTOUser?
                                                = null,

        @Json(name                              = "assignee")
        @ColumnInfo(name                        = "assignee")
        var assignee                            : CDTOUser?
                                                = null,

        @Json(name                              = "acceptanceDate")
        @ColumnInfo(name                        = "acceptanceDate")
        var acceptanceDate                      : LocalDateTime?
                                                = null,

        @Json(name                              = "assignmentDate")
        @ColumnInfo(name                        = "assignmentDate")
        var assignmentDate                      : LocalDateTime?
                                                = null,

        @Json(name                              = "endDate")
        @ColumnInfo(name                        = "endDate")
        var endDate                             : LocalDateTime?
                                                = null,

        @Json(name                              = "sequenceNumber")
        @ColumnInfo(name                        = "sequenceNumber")
        var sequenceNumber                      : Int?
                                                = null,

        @Json(name                              = "object")
        @ColumnInfo(name                        = "object")
        var `object`                            : CDTOObject?
                                                = null
//
//        @Json(name                              = "comments")
//        @ColumnInfo(name                        = "comments")
//        var comments                            : CDTOObject?
//                                                = null,
//
//        @Json(name                              = "taskAttachment")
//        @ColumnInfo(name                        = "taskAttachment")
//        var taskAttachment                      : CDTOObject?
//                                                = null

        )                                           : CNamedObject()
{
        fun toTask()                        : CTask
        {
            val task                        = CTask(
                deadline,
                status?.id,
                `object`?.id,
                creator?.id,
                initiator?.id,
                assignee?.id,
                acceptanceDate,
                assignmentDate,
                endDate,
                sequenceNumber)
            task.id                         = id
            task.description                = description
            task.createdAt                  = createdAt
            task.updatedAt                  = updatedAt

            return task
        }
}
