package ru.psu.mobileapp.di.modules

import android.content.Context
import android.util.Log
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import net.openid.appauth.AuthState
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit.converter.java8.Java8OptionalConverterFactory
import ru.psu.mobileapp.utils.network.api.CServiceServerAPI
import ru.psu.mobileapp.utils.network.api.IServiceServerAPI
import ru.psu.mobileapp.utils.network.api.IServerAPITemplate
import ru.psu.mobileapp.utils.properties.IServiceProperties
import javax.inject.Named
import javax.inject.Singleton

/********************************************************************************************************
 * Модуль Dagger позволяет внедрять ссылки на функционал обмена данными с API сервера.                  *
 * https://medium.com/@marco_cattaneo/integrate-dagger-2-with-room-persistence-library-in-              *
 * few-lines-abf48328eaeb                                                                               *
 * https://proandroiddev.com/mvvm-with-kotlin-android-architecture-components-dagger-2-retrofit-        *
 * and-rxandroid-1a4ebb38c699                                                                           *
 * @author Селетков И.П. 2018 0816.                                                                     *
 *******************************************************************************************************/
@Module
@Suppress("unused")
class CModuleServerAPI
{
    @Provides
//    @Singleton
    internal  fun provideAuthState(
        context : Context
    ) : AuthState
    {
        val authPrefs = context.getSharedPreferences("OktaAppAuthState", Context.MODE_PRIVATE)
        val stateJson = authPrefs.getString("state", "")
        return if (!stateJson!!.isEmpty()) {
            try {
                AuthState.jsonDeserialize(stateJson)
            } catch (exp: org.json.JSONException) {
               // Log.e("ERROR",exp.message)
                AuthState()
            }

        } else {
            AuthState()
        }

    }

    /****************************************************************************************************
     * Возвращает объект из библиотеки Retrofit.                                                        *
     * @return объект Retrofit.                                                                         *
     ***************************************************************************************************/
    @Provides
    @Singleton
    @Named("DataServerAPI")
    internal fun provideRetrofitInterface(
        moshi                               : Moshi,
        okHttpClient                        : OkHttpClient,
        properties                          : IServiceProperties
    )                                       : Retrofit
    {
        val url                             = properties.get("ru.psu.mobileapp.mvvm.API.address")

        url ?: throw Exception("E0001. Server API URL is not specified.")
        return Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(Java8OptionalConverterFactory.create())
                    .addConverterFactory(MoshiConverterFactory.create(moshi))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .client(okHttpClient)
                    .build()
    }
    /****************************************************************************************************
     * Построение шаблона запросов Retrofit для работы с API сервера.                                   *
     * @param retrofit - объект из библиотеки Retrofit, позволяющий создать объект работы с API по      *
     * его интерфейсу.                                                                                  *
     * @return шаблон запросов к API.                                                                   *
     ***************************************************************************************************/
    @Provides
    @Singleton
    internal fun provideServerAPITemplate(
        @Named("DataServerAPI") retrofit
                                            : Retrofit
    )                                       : IServerAPITemplate
    {
        return retrofit.create(IServerAPITemplate::class.java)
    }
    /****************************************************************************************************
     * Построение сервиса для работы с API сервера.                                                     *
     * @param template - шаблон запросов retrofit к конкретному API.                                    *
     * @return сервис для работы с API.                                                                 *
     ***************************************************************************************************/
    @Provides
    @Singleton
    internal fun provideServerAPIService(
        template                            : IServerAPITemplate,
        authState                           : AuthState
    )                                       : IServiceServerAPI
    {
        return CServiceServerAPI(template, authState)
    }
}

