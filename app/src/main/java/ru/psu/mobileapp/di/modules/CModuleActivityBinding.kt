package ru.psu.mobileapp.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.psu.mobileapp.di.modules.activities.CModuleActivityTaskDetails
import ru.psu.mobileapp.di.modules.activities.CModuleActivityMain
import ru.psu.mobileapp.di.modules.activities.CModuleActivityTasks
import ru.psu.mobileapp.di.scopes.CScopeActivity
import ru.psu.mobileapp.view.activities.CActivityTaskDetails
import ru.psu.mobileapp.view.activities.CActivityMain
import ru.psu.mobileapp.view.activities.CActivityTasks

@Module
abstract class CModuleActivityBinding
{
//    @CScopeActivity
//    @ContributesAndroidInjector(
//        modules                             = [
//            CModuleActivityLauncher::class
//        ]
//    )
//    internal abstract fun bindActivityLauncher(
//
//    )                                       : CActivityLauncher

    @CScopeActivity
    @ContributesAndroidInjector(
        modules                             = [
            CModuleActivityTasks::class
                    ]
    )

    internal abstract fun bindAcivityTasks(
    )                                       : CActivityTasks

    @CScopeActivity
    @ContributesAndroidInjector(
        modules                             = [
            CModuleActivityMain::class
        ]
    )
    internal abstract fun bindActivityMain(
    )                                       : CActivityMain

    @CScopeActivity
    @ContributesAndroidInjector(
        modules                             = [
            CModuleActivityTaskDetails::class
        ]
    )
    internal abstract fun bindActivityTaskDetails(
    )                                       : CActivityTaskDetails







}