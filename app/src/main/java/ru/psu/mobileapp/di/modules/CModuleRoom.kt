package ru.psu.mobileapp.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import ru.psu.mobileapp.data.dao.*
import ru.psu.mobileapp.utils.database.CRoomDatabase
import javax.inject.Singleton

/********************************************************************************************************
 * Модуль Dagger позволяет внедрять ссылки на объект-базу данных в места, где это запрашивается.        *
 * @see "https://medium.com/@marco_cattaneo/integrate-dagger-2-with-room-persistence-library-in-        *
 * few-lines-abf48328eaeb"                                                                              *
 * @author Селетков И.П. 2018 0816.                                                                     *
 *******************************************************************************************************/
@Module
@Suppress("unused")
class CModuleRoom
{
    /****************************************************************************************************
     * Возвращает ссылку на базу данных.                                                                *
     * @param context - контекст выполнения приложения.                                                 *
     ***************************************************************************************************/
    @Singleton
    @Provides
    fun providesRoomDatabase(
        context                             : Context
    )                                       : CRoomDatabase
    {
        return CRoomDatabase.getInstance(context)
    }

    /****************************************************************************************************
     * Возвращает ссылку на объект доступа к данным типа CAttributeValue.                               *
     * @param database - база данных, открытая с помощью room.                                          *
     ***************************************************************************************************/
    @Singleton
    @Provides
    fun providesAttributeValueDAO(
        database                            : CRoomDatabase
    )                                       : IDAOSubject
    {
        return database.daoSubject()
    }
    /****************************************************************************************************
     * Возвращает ссылку на объект доступа к данным типа CAttributeValue.                               *
     * @param database - база данных, открытая с помощью room.                                          *
     ***************************************************************************************************/
    @Singleton
    @Provides
    fun providesDAOWorkSpace(
        database                            : CRoomDatabase
    )                                       : IDAOWorkSpace
    {
        return database.daoWorkSpace()
    }

    /****************************************************************************************************
     * Возвращает ссылку на объект доступа к данным типа CAttributeValue.                               *
     * @param database - база данных, открытая с помощью room.                                          *
     ***************************************************************************************************/
    @Singleton
    @Provides
    fun providesDAOTask(
        database                            : CRoomDatabase
    )                                       : IDAOTask
    {
        return database.daoTask()
    }

    /****************************************************************************************************
     * Возвращает ссылку на объект доступа к данным типа CAttributeValue.                               *
     * @param database - база данных, открытая с помощью room.                                          *
     ***************************************************************************************************/
    @Singleton
    @Provides
    fun providesDAOStatus(
        database                            : CRoomDatabase
    )                                       : IDAOStatus
    {
        return database.daoStatus()
    }

    /****************************************************************************************************
     * Возвращает ссылку на объект доступа к данным типа CAttributeValue.                               *
     * @param database - база данных, открытая с помощью room.                                          *
     ***************************************************************************************************/
    @Singleton
    @Provides
    fun providesDAOObject(
        database                            : CRoomDatabase
    )                                       : IDAOObject
    {
        return database.daoObject()
    }

    /****************************************************************************************************
     * Возвращает ссылку на объект доступа к данным типа CAttributeValue.                               *
     * @param database - база данных, открытая с помощью room.                                          *
     ***************************************************************************************************/
    @Singleton
    @Provides
    fun providesDAOUser(
        database                            : CRoomDatabase
    )                                       : IDAOUser
    {
        return database.daoUser()
    }
}