package ru.psu.mobileapp.di.components

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import ru.psu.mobileapp.CApplication
import ru.psu.mobileapp.di.modules.*
import javax.inject.Singleton

/********************************************************************************************************
 * Компонент dagger 2, отвечает за хранение и внедрение сылок на все объекты, описанные в модулях.      *
 * https://medium.com/@Zhuinden/that-missing-guide-how-to-use-dagger2-ef116fbea97                       *
 * @author Селетков И.П. 2018 0816.                                                                     *
 *******************************************************************************************************/
@Singleton
@Component(
    modules                                 = [
        AndroidSupportInjectionModule::class,
        CModuleApplication::class,
        CModuleNetworkTools::class,
        CModuleServerAPI::class,
        CModuleDataService::class,
        CModuleViewModel::class,
        CModuleActivityBinding::class,
        CModuleRoom::class
    ]
)
interface IComponentApp                     : AndroidInjector<CApplication>
{
    @Component.Builder
    abstract class Builder                  : AndroidInjector.Builder<CApplication>() {}
}