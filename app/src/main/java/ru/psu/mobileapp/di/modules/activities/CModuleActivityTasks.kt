package ru.psu.mobileapp.di.modules.activities

import dagger.Module
import dagger.Provides
import ru.psu.mobileapp.view.activities.CActivityTasks

@Module
class CModuleActivityTasks
{
    @Provides
    fun provideActivityTasks(
        activityTasks                        : CActivityTasks
    )                                        : CActivityTasks
    {
        return activityTasks
    }
}