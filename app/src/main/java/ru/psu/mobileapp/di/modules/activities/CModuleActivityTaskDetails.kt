package ru.psu.mobileapp.di.modules.activities

import dagger.Module
import dagger.Provides
import ru.psu.mobileapp.view.activities.CActivityTaskDetails

@Module
class CModuleActivityTaskDetails {
    @Provides
    fun provideActivityTaskDetils(
        activityTaskDetails                  : CActivityTaskDetails
    )                                        : CActivityTaskDetails
    {
        return activityTaskDetails
    }
}