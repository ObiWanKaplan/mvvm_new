package ru.psu.mobileapp.data.repositories

import io.reactivex.Flowable
import ru.psu.mobileapp.data.CResource
import ru.psu.mobileapp.model.CTask
import java.util.*

interface IRepositoryTask                : IRepositoryBase<CTask, UUID>
{
    fun getListFromServer(
        workspaceId                         : UUID
    )                                       : Flowable<CResource<List<CTask>>>
}