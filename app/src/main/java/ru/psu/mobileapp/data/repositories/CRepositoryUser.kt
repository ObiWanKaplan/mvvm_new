package ru.psu.mobileapp.data.repositories

import android.content.Context
import android.util.Log
import io.reactivex.Flowable
import ru.psu.mobileapp.data.CNetworkBoundResource
import ru.psu.mobileapp.data.CResource
import ru.psu.mobileapp.data.dao.IDAOTask
import ru.psu.mobileapp.data.dao.IDAOUser
import ru.psu.mobileapp.model.CTask
import ru.psu.mobileapp.model.CUser
import ru.psu.mobileapp.utils.network.api.IServiceServerAPI
import java.util.*
import javax.inject.Inject

class CRepositoryUser
    /********************************************************************************************************
     * Конструктор.                                                                                         *
     *******************************************************************************************************/
    @Inject
    constructor (
        private val context: Context,
        private val daoUser: IDAOUser,
        private val serviceServerAPI: IServiceServerAPI

    ) :
    CRepositoryBase<CUser, UUID>(daoUser),
    IRepositoryUser
        /****************************************************************************************************
         * Возвращает список всех имеющихся поруений с идентификатором [workSpaceId]                        *
         * @return - список значений атрибута.                                                              *
         ***************************************************************************************************/
{
        override fun saveListFromServerToDB(
            workSpaceId                     : UUID
        )                                   : Flowable<CResource<List<CUser>>>
        {
            return object : CNetworkBoundResource<List<CUser>, List<CUser>>() {

                override fun saveCallResult(
                    item: List<CUser>
                ) {
                    item
                        .forEach {
                            try {

                                daoUser.insert(it)

                            } catch (e: Exception) {
                                val a = 0
                            }
                        }
                }

                override fun shouldFetch(
                ): Boolean {
                    return true
                }

                override fun loadFromDB(
                ): Flowable<List<CUser>> {
                    return Flowable.just(listOf())
                }

                override fun createCall(

                ): Flowable<CResource<List<CUser>>> {
//                return Flowable.just(
//                            CResource.success(listOf())
//                        )
                    return serviceServerAPI.fetchUsers(workSpaceId)
                        .map { list ->
                            list.map {
                                it.toUser()
                            }
                        }
                        .flatMapPublisher { list ->
                            Flowable.just(
                                CResource.success(list)
                            )
                        }
                }
            }
                .flowable
                .doOnError {
                    Log.e("PP", "Error while fetching subjects from server", it)
                }
        }
}