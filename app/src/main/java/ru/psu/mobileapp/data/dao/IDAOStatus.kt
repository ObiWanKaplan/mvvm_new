package ru.psu.mobileapp.data.dao

import androidx.room.Dao
import androidx.room.Query
import io.reactivex.Flowable
import io.reactivex.Single
import ru.psu.mobileapp.model.CStatus
import ru.psu.mobileapp.model.CTask
import java.util.*


@Dao
interface IDAOStatus                : IDAOBase<CStatus, UUID>
{
    /****************************************************************************************************
     * Возвращает список всех объектов типа [CSubject].                                                 *
     *                                                                                                  *
     * @return - список объектов с возможностью отслеживания изменений.                                 *
     ***************************************************************************************************/
    @Query("""
        SELECT *
        FROM statuses
    """)
    override fun get()                      : Single<List<CStatus>>

    /****************************************************************************************************
     * Возвращает список всех объектов типа [CSubject], которые есть в базе данных с                    *
     * возможностью отслеживания изменений в составе списка.                                            *
     *                                                                                                  *
     * @return - список объектов с возможностью отслеживания изменений.                                 *
     ***************************************************************************************************/
    @Query("""
        SELECT *
        FROM statuses
    """)
    override fun getFlowable()              : Flowable<List<CStatus>>
    /*****************************************************************************************************
     * Возвращает объект типа [CSubject] по идентификатору [id].                                        *
     *                                                                                                  *
     * @param id - идентификатор объекта.                                                               *
     * @return - обёртка над объектом с возможностью отслеживания изменений.                            *
     ***************************************************************************************************/
    @Query("""
        SELECT *
        FROM statuses
        WHERE id                            = :id
        LIMIT 1
    """)
    override fun get(
        id                                  : UUID
    )                                       : Single<CStatus>
    /****************************************************************************************************
     * Возвращает объект типа [CSubject] по идентификатору [id] с возможностью отслеживания             *
     * изменений объекта.                                                                               *
     *                                                                                                  *
     * @param id - идентификатор объекта.                                                               *
     * @return - обёртка над объектом с возможностью отслеживания изменений.                            *
     ***************************************************************************************************/
    @Query("""
        SELECT *
        FROM statuses
        WHERE id                            = :id
        LIMIT 1
    """)
    override fun getFlowable(
        id                                  : UUID
    )                                       : Flowable<CStatus>
    /****************************************************************************************************
     * Возвращает объект типа [CSubject] по идентификатору [id].                                        *
     *                                                                                                  *
     * @param id - идентификатор объекта.                                                               *
     * @return - обёртка над объектом с возможностью отслеживания изменений.                            *
     ***************************************************************************************************/
    @Query("""
        SELECT *
        FROM statuses
        WHERE id                            = :id
        LIMIT 1
    """)
    override fun getSync(
        id                                  : UUID
    )                                       : CStatus?

    /****************************************************************************************************
     * Возвращает список всех объектов типа [CSubject].                                                 *
     *                                                                                                  *
     * @return - список объектов с возможностью отслеживания изменений.                                 *
     ***************************************************************************************************/
    @Query("""
        SELECT *
        FROM statuses
    """)
    fun getName()                      : List<CStatus>
}