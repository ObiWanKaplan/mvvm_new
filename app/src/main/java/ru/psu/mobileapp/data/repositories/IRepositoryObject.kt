package ru.psu.mobileapp.data.repositories

import io.reactivex.Flowable
import ru.psu.mobileapp.data.CResource
import ru.psu.mobileapp.model.CObject
import java.util.*

interface IRepositoryObject                 : IRepositoryBase<CObject, UUID>
{
    fun getListFromServer(
        workspaceId                         : UUID
    )                                       : Flowable<CResource<List<CObject>>>
}