package ru.psu.mobileapp.data.repositories

import android.content.Context
import android.util.Log
import io.reactivex.Flowable
import ru.psu.mobileapp.data.CNetworkBoundResource
import ru.psu.mobileapp.data.CResource
import ru.psu.mobileapp.data.dao.IDAOObject
import ru.psu.mobileapp.model.CObject
import ru.psu.mobileapp.utils.network.api.IServiceServerAPI
import java.util.*
import javax.inject.Inject

class CRepositoryObject
/********************************************************************************************************
 * Конструктор.                                                                                         *
 *******************************************************************************************************/
@Inject
constructor (
    private val context                     : Context,
    private val daoObject                   : IDAOObject,
    private val serviceServerAPI            : IServiceServerAPI

)                                             :
    CRepositoryBase<CObject, UUID>(daoObject),
    IRepositoryObject
{
    /****************************************************************************************************
     * Возвращает список всех имеющихся статусов.                                                       *
     * @return - список значений статусов.                                                              *
     ***************************************************************************************************/
    override fun getListFromServer(
        workspaceId                         : UUID
    )                                       : Flowable<CResource<List<CObject>>>
    {

        return object                       : CNetworkBoundResource<List<CObject>, List<CObject>>() {

            override fun saveCallResult(
                item                        : List<CObject>)
            {
                item
                    .asSequence()
                    .forEach {
                        daoObject.insert(it)
                    }

//                try {
//                    daoStatus.insert(list[0])
//                    //daoStatus.insert(list)
//                }
//                catch (e: Exception)
//                {
//                    val a=0
//                }
            }

            override fun shouldFetch(
            )                               : Boolean
            {
                return true
            }

            override fun loadFromDB(
            )                               : Flowable<List<CObject>>
            {
                return daoObject.getByWorkSpaceFlowable(workspaceId.toString())
            }

            override fun createCall(

            )                               : Flowable<CResource<List<CObject>>>
            {
                return serviceServerAPI.fetchObjects(workspaceId)
                    .map {list ->
                        list.map{
                            it.toObject()
                        }
                    }
                    .flatMapPublisher {
                            list ->
                        Flowable.just(
                            CResource.success(list)
                        )
                    }
            }
        }
            .flowable
            .doOnError {
                Log.e("PP", "Error while fetching subjects from server", it)
            }
    }
}