package ru.psu.mobileapp.data.repositories

import android.content.Context
import android.util.Log
import io.reactivex.Flowable
import ru.psu.mobileapp.data.CNetworkBoundResource
import ru.psu.mobileapp.data.CResource
import ru.psu.mobileapp.data.dao.IDAOTask
import ru.psu.mobileapp.model.CTask
import ru.psu.mobileapp.utils.network.api.IServiceServerAPI
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CRepositoryTask
/********************************************************************************************************
 * Конструктор.                                                                                         *
 *******************************************************************************************************/
@Inject
constructor (
    private val context                     : Context,
    private val daoTask                     : IDAOTask,
    private val serviceServerAPI            : IServiceServerAPI

)                                             :
    CRepositoryBase<CTask, UUID>(daoTask),
    IRepositoryTask
{
    /****************************************************************************************************
     * Возвращает список всех имеющихся поруений с идентификатором [workSpaceId]                        *
     * @return - список значений атрибута.                                                              *
     ***************************************************************************************************/
    override fun getListFromServer(
        workSpaceId                         : UUID
    )                                       : Flowable<CResource<List<CTask>>>
    {
        return object                       : CNetworkBoundResource<List<CTask>, List<CTask>>() {

            override fun saveCallResult(
                item                        : List<CTask>)
            {
                item
                    .forEach {
                try {

                            daoTask.insert(it)

                }
                catch(e :Exception)
                {
                    val a= 0
                }
                    }
            }

            override fun shouldFetch(
            )                               : Boolean
            {
                return true
            }

            override fun loadFromDB(
            )                               : Flowable<List<CTask>>
            {
                return daoTask.getByWorkspaceFlowable(workSpaceId.toString())
//                return daoTask.getFlowable()
            }

            override fun createCall(

            )                               : Flowable<CResource<List<CTask>>>
            {
//                return Flowable.just(
//                            CResource.success(listOf())
//                        )
                return serviceServerAPI.fetchTasks(workSpaceId)
                    .map {list ->
                        list.map{
                            it.toTask()
                        }
                    }
                    .flatMapPublisher {
                            list ->
                        Flowable.just(
                            CResource.success(list)
                        )
                    }
            }
        }
            .flowable
            .doOnError {
                Log.e("PP", "Error while fetching subjects from server", it)
            }
    }
}