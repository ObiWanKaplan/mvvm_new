package ru.psu.mobileapp.data.repositories

import android.content.Context
import io.reactivex.Single
import ru.psu.mobileapp.data.dao.IDAOWorkSpace
import ru.psu.mobileapp.model.CWorkSpace
import ru.psu.mobileapp.utils.network.api.IServiceServerAPI
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CRepositoryWorkSpace
/********************************************************************************************************
 * Конструктор.                                                                                         *
 *******************************************************************************************************/
@Inject
constructor (
    private val context                     : Context,
    private val daoWorkSpace                : IDAOWorkSpace,
    private val serviceServerAPI            : IServiceServerAPI

)                                             :
    CRepositoryBase<CWorkSpace, UUID>(daoWorkSpace),
    IRepositoryWorkSpace
{
    /****************************************************************************************************
     * Возвращает список всех имеющихся значений атрибута с идентификатором [attributeId] по            *
     * контрольной точке [checkPointId].                                                                *
                                                  *
     * @return - список значений атрибута.                                                              *
     ***************************************************************************************************/
    override fun getListFromServer(
    )                                       : Single<List<CWorkSpace>>
    {
        return serviceServerAPI.fetchWorkSpaces()
    }
}