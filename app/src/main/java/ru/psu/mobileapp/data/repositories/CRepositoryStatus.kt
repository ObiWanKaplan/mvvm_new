package ru.psu.mobileapp.data.repositories

import android.content.Context
import android.util.Log
import io.reactivex.Flowable
import ru.psu.mobileapp.data.CNetworkBoundResource
import ru.psu.mobileapp.data.CResource
import ru.psu.mobileapp.data.dao.IDAOStatus
import ru.psu.mobileapp.model.CStatus
import ru.psu.mobileapp.utils.network.api.IServiceServerAPI
import java.util.*
import javax.inject.Inject

class CRepositoryStatus
/********************************************************************************************************
 * Конструктор.                                                                                         *
 *******************************************************************************************************/
@Inject
constructor (
    private val context                     : Context,
    private val daoStatus                   : IDAOStatus,
    private val serviceServerAPI            : IServiceServerAPI

)                                             :
    CRepositoryBase<CStatus, UUID>(daoStatus),
    IRepositoryStatus
{
    /****************************************************************************************************
     * Возвращает список всех имеющихся статусов.                                                       *
     * @return - список значений статусов.                                                              *
     ***************************************************************************************************/
    override fun getListFromServer(
    )                                       : Flowable<CResource<List<CStatus>>>
    {

        return object                       : CNetworkBoundResource<List<CStatus>, List<CStatus>>() {

            override fun saveCallResult(
                item                        : List<CStatus>)
            {
                item
                    .asSequence()
                    .forEach {
                        daoStatus.insert(it)
                    }
            }

            override fun shouldFetch(
            )                               : Boolean
            {
                return true
            }

            override fun loadFromDB(
            )                               : Flowable<List<CStatus>>
            {
                return daoStatus.getFlowable()
            }

            override fun createCall(

            )                               : Flowable<CResource<List<CStatus>>>
            {
                return serviceServerAPI.fetchStatuses()
                    .flatMapPublisher {
                            list ->
                        Flowable.just(
                            CResource.success(list)
                        )
                    }
            }
        }
            .flowable
            .doOnError {
                Log.e("PP", "Error while fetching subjects from server", it)
            }
    }
}