package ru.psu.mobileapp.data.repositories

import ru.psu.mobileapp.model.CSubject
import java.util.*

/********************************************************************************************************
 * Интерфейс описывает все методы бизнес-логики, которые могут применятся к объектам типа               *
 * [CSubject].                                                                                       *
 * @author Селетков И.П. 2019 0211.                                                                     *
 *******************************************************************************************************/
interface IRepositorySubject                : IRepositoryBase<CSubject, UUID>
{
//    /****************************************************************************************************
//     * Возвращает список атрибутов по контрольному листу с идентификатором [checkListId].               *
//     *                                                                                                  *
//     * @return список атрибутов контрольного листа.                                                     *
//     ***************************************************************************************************/
//    fun getByCheckListSync(
//        checkListId                         : UUID
//    )                                       : List<CCheckListAttribute>
}