package ru.psu.mobileapp.data.repositories

import io.reactivex.Flowable
import ru.psu.mobileapp.data.CResource
import ru.psu.mobileapp.model.CUser
import java.util.*

    interface IRepositoryUser               : IRepositoryBase<CUser, UUID>
    {
        fun saveListFromServerToDB(
            workSpaceId                     : UUID
        )                                   : Flowable<CResource<List<CUser>>>
    }