package ru.psu.mobileapp.data.repositories

import io.reactivex.Flowable
import ru.psu.mobileapp.data.CResource
import ru.psu.mobileapp.model.CStatus
import java.util.*

interface IRepositoryStatus                    : IRepositoryBase<CStatus, UUID>
{
    fun getListFromServer()                 : Flowable<CResource<List<CStatus>>>
}