package ru.psu.mobileapp.data.repositories

import io.reactivex.Single
import ru.psu.mobileapp.model.CWorkSpace
import java.util.*

interface IRepositoryWorkSpace                 : IRepositoryBase<CWorkSpace, UUID>
{
    fun getListFromServer(
    )                                       : Single<List<CWorkSpace>>
}