package ru.psu.mobileapp.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.LocalDateTime
import ru.psu.mobileapp.data.repositories.IRepositoryObject
import ru.psu.mobileapp.data.repositories.IRepositoryStatus
import ru.psu.mobileapp.data.repositories.IRepositoryTask
import ru.psu.mobileapp.data.repositories.IRepositoryUser
import ru.psu.mobileapp.dto.CDTOTask
import ru.psu.mobileapp.model.CObject
import ru.psu.mobileapp.model.CStatus
import ru.psu.mobileapp.model.CTask
import ru.psu.mobileapp.model.CUser
import ru.psu.mobileapp.utils.converters.CConverterLocalDateTime
import ru.psu.mobileapp.utils.network.api.IServiceServerAPI
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CViewModelActivityTaskDetails
/********************************************************************************************************
 * Конструктор.                                                                                         *
 *******************************************************************************************************/
@Inject
constructor
    (
    val context                                 : Context,
    val repositoryTask                          : IRepositoryTask,
    val repositoryObject                        : IRepositoryObject,
    val repositoryUser                          : IRepositoryUser,
    val repositoryStatus                        : IRepositoryStatus,
    private val serviceServerAPI                : IServiceServerAPI
)                                               : CViewModelBase() {
    private val TAG = "CViewModelActivityTaskDetails"
    //Список дисциплин, отображаемых на главной активности.
    val task = MutableLiveData<CTask>()
    val deadline = MutableLiveData<String>()

    val creatorName                         = MutableLiveData<String>()
    val initiatorName                       = MutableLiveData<String>()
    val assigneeName                        = MutableLiveData<String>()
    val acceptanceDate                      = MutableLiveData<String>()
    val assignmentDate                       = MutableLiveData<String>()
    val endDate                             = MutableLiveData<String>()
    val createdAt                           = MutableLiveData<String>()
    val updatedAt                           = MutableLiveData<String>()
    val status: String? = null
    val sequenceNumber                      = MutableLiveData<String>()
//  val objectName                          = MutableLiveData<String>()
    val active: Boolean? = false
    private val allStatuses                 = MutableLiveData<List<CStatus>>()

    val allUsers                            = MutableLiveData<List<CUser>>()
    val allStatuses1                        = MutableLiveData<List<CStatus>>()
    val allObjects                          = MutableLiveData<List<CObject>>()

    val selectedObjectNum                   = MutableLiveData<Int>()
    val selectedInitiatorNum                = MutableLiveData<Int>()
    val selectedStatusNum                   = MutableLiveData<Int>()


    //Признак того, что происходит актуализация данных.
    //Влияет на видимость индикатора
    val loading = MutableLiveData<Boolean>()

    private var _taskId: UUID? = null
    var taskId: UUID
        get() = _taskId!!
        set(value) {
            _taskId = value
            loading.value = true

            //TODO убрать множественный запрос задачи в БД (добавить кэштрование)

            val singleTask = repositoryTask.get(value)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CTask()
                }

            val singleTaskNew = singleTask
                .doOnSuccess {
                    task.postValue(it)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CTask()
                }

            val singleObject = singleTask
                .filter { it.objectId != null }
                .flatMapSingle {
                    repositoryObject.get(it.objectId!!)
                        .subscribeOn(Schedulers.io())
                }
                .delay(200, TimeUnit.MILLISECONDS)
                .doOnSuccess {
                    //objectName.postValue(it.name)

                    selectedObjectNum.postValue(allObjects.value?.indexOf(it) ?: 0)

                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CObject()
                }

            val singleStatus = singleTask
                .filter { it.objectId != null }
                .flatMapSingle {
                    repositoryStatus.get(it.statusId!!)
                        .subscribeOn(Schedulers.io())
                }
                .delay(200, TimeUnit.MILLISECONDS)
                .doOnSuccess {
                    //objectName.postValue(it.name)

                    selectedStatusNum.postValue(allStatuses1.value?.indexOf(it) ?: 0)

                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CStatus()
                }

            val singleInitiator = singleTask
                .filter { it.initiatorId != null }
                .flatMapSingle {
                    repositoryUser.get(it.initiatorId!!)
                        .subscribeOn(Schedulers.io())
//                        .doOnError { e ->
//                            Log.e("PP", "Error while fetching by assignee id ${task.initiatorId}", e)
//                        }
//                        .onErrorReturnItem(CUser())
        }
                .delay(200, TimeUnit.MILLISECONDS)
                .doOnSuccess {
                    selectedInitiatorNum.postValue(allUsers.value?.indexOf(it) ?: 0)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CUser()
                }

            val singleCreator = singleTask
                .filter { it.creatorId != null }
                .flatMapSingle { task ->
                    repositoryUser.get(task.creatorId!!)
                        .subscribeOn(Schedulers.io())
                        .doOnError { e ->
                            Log.e("PP", "Error while fetching by assignee id ${task.creatorId}", e)
                        }
                        .onErrorReturnItem(CUser())
                }
                .doOnSuccess {
                    creatorName.postValue(it.Name())
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CUser()
                }


            val singleAssignee = singleTask
                .filter { it.assigneeId != null }
                .flatMapSingle { task ->
                    repositoryUser.get(task.assigneeId!!)
                        .subscribeOn(Schedulers.io())
                        .doOnError { e ->
                            Log.e("PP", "Error while fetching by assignee id ${task.assigneeId}", e)
                        }
                        .onErrorReturnItem(CUser())
                }
                .doOnSuccess {
                    assigneeName.postValue(it.Name())
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CUser()
                }

            val singleAcceptanceDate = singleTask
                .filter { it.acceptanceDate != null }
                .doOnSuccess {
                    val a = it.acceptanceDate
                    val b = CConverterLocalDateTime.dateToString(a)
                    acceptanceDate.postValue(b)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CTask()
                }

            val singleAssignmentDate = singleTask
               // .filter { it.assignmentDate != null }
                .doOnSuccess {
                    val a = it.assignmentDate
                    val b = CConverterLocalDateTime.dateToString(a)
                    assignmentDate.postValue(b)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CTask()
                }

            val singleEndDate = singleTask
                .filter { it.endDate != null }
                .doOnSuccess {
                    val a = it.endDate
                    val b = CConverterLocalDateTime.dateToString(a)
                    endDate.postValue(b)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CTask()
                }

            val singleCreatedAt = singleTask
                .filter { it.createdAt != null }
                .doOnSuccess {
                    val a = it.createdAt
                    val b = CConverterLocalDateTime.dateToString(a)
                    createdAt.postValue(b)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CTask()
                }

            val singleUpdatedAt = singleTask
                .filter { it.updatedAt != null }
                .doOnSuccess {
                    val a = it.updatedAt
                    val b = CConverterLocalDateTime.dateToString(a)
                    updatedAt.postValue(b)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CTask()
                }

            val singleSequenceNumber = singleTask
                .filter { it.sequenceNumber != null }
                .doOnSuccess {
                    sequenceNumber.postValue(it.sequenceNumber.toString())
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn {
                    CTask()
                }


            registerDisposable(singleTaskNew.subscribe())
            registerDisposable(singleObject.subscribe())
            registerDisposable(singleStatus.subscribe())
            registerDisposable(singleInitiator.subscribe())
            registerDisposable(singleCreator.subscribe())
            registerDisposable(singleAssignee.subscribe())
            registerDisposable(singleAcceptanceDate.subscribe())
            registerDisposable(singleAssignmentDate.subscribe())
            registerDisposable(singleEndDate.subscribe())
            registerDisposable(singleCreatedAt.subscribe())
            registerDisposable(singleUpdatedAt.subscribe())
            registerDisposable(singleSequenceNumber.subscribe())
        }

    init {
        registerDisposable(repositoryStatus.getListFromServer()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {

                    resource ->
                allStatuses.postValue(resource.data)
            }
        )

        registerDisposable(
            repositoryUser.get()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {

                        list ->
                    allUsers.postValue(list)
                }
        )

        registerDisposable(
            repositoryStatus.get()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {

                        list ->
                    allStatuses1.postValue(list)
                }
        )

        registerDisposable(
            repositoryObject.get()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                        list ->
                    allObjects.postValue(list)
                }
        )


    }

    fun getName(name: MutableLiveData<String>): String {
        return name.value!!
    }

    fun getDescription(task: MutableLiveData<CTask>): String {
        val test = task.value
        if (test == null) return ""
        else
            return test.description!!

    }

    fun updateAssignmentDate(year : Int, month : Int, day : Int)
    {
        val x = LocalDateTime.of(year, month, day, 0, 0)


        //val b = CConverterLocalDateTime.stringToDate(year + "-" + month + "-" + day)
        task.value?.let {
            it.assignmentDate = x
            registerDisposable(
                Single.fromCallable{
                    repositoryTask.save(it)
                }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
            )

        }
        assignmentDate.postValue(CConverterLocalDateTime.dateToString(x))
    }


    fun updateDeadline(year : Int, month : Int, day : Int)
    {
        val x = LocalDateTime.of(year, month, day, 0, 0)


        //val b = CConverterLocalDateTime.stringToDate(year + "-" + month + "-" + day)
        task.value?.let {
            it.deadline = x
            registerDisposable(
                Single.fromCallable{
                    repositoryTask.save(it)
                }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
            )

        }
        deadline.postValue(CConverterLocalDateTime.dateToString(x))
    }


}

