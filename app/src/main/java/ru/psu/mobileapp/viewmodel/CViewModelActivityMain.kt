package ru.psu.mobileapp.viewmodel

import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.psu.mobileapp.data.dao.IDAOStatus
import ru.psu.mobileapp.data.repositories.IRepositoryWorkSpace
import ru.psu.mobileapp.di.modules.CModuleRoom_ProvidesDAOStatusFactory
import ru.psu.mobileapp.model.CWorkSpace
import java.util.*
import javax.inject.Inject


/********************************************************************************************************
 * Модель основного представления.                                                                      *
 * @author Селетков И.П. 2018 0925.                                                                     *
 *******************************************************************************************************/
class CViewModelActivityMain
/********************************************************************************************************
 * Конструктор.                                                                                         *
 *******************************************************************************************************/
@Inject
constructor
(
    val serviceWorkSpace                    : IRepositoryWorkSpace
)                                           : CViewModelBase()
{

    private val TAG = "CViewModelActivityMain"
    //Список дисциплин, отображаемых на главной активности.
    val workspaces                           = MutableLiveData<List<CWorkSpace>>()
    //Признак того, что происходит актуализация данных.
    //Влияет на видимость индикатора

    val loading                             = MutableLiveData<Boolean>()


    //Начальные значения всех полей.
    init{

        loading.value                       = true

        registerDisposable(serviceWorkSpace.getListFromServer()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                list -> workspaces.postValue(list)
            }
        )
    }


}