package ru.psu.mobileapp.viewmodel

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.Flowables
import org.threeten.bp.LocalDateTime
import ru.psu.mobileapp.R
import ru.psu.mobileapp.data.dao.IDAOStatus
import ru.psu.mobileapp.data.repositories.IRepositoryObject
import ru.psu.mobileapp.data.repositories.IRepositoryStatus
import ru.psu.mobileapp.data.repositories.IRepositoryTask
import ru.psu.mobileapp.data.repositories.IRepositoryUser
import ru.psu.mobileapp.model.CObject
import ru.psu.mobileapp.model.CStatus
import ru.psu.mobileapp.model.CTask
import ru.psu.mobileapp.model.CUser
import ru.psu.mobileapp.utils.converters.CConverterLocalDateTime
import ru.psu.mobileapp.utils.network.api.IServiceServerAPI
import java.util.*
import javax.inject.Inject

/********************************************************************************************************
 * Модель основного представления.                                                                      *
 * @author Селетков И.П. 2018 0925.                                                                     *
 *******************************************************************************************************/
class CViewModelActivityTasks
/********************************************************************************************************
 * Конструктор.                                                                                         *
 *******************************************************************************************************/
@Inject
constructor
(
    val context                             : Context,
    val repositoryTask                      : IRepositoryTask,
    val repositoryStatus                    : IRepositoryStatus,
    val repositoryObject                    : IRepositoryObject,
    val repositoryUser                      : IRepositoryUser,
    val daoStatus                           : IDAOStatus,
    private val serviceServerAPI            : IServiceServerAPI
)                                           : CViewModelBase()
{
    private val TAG = "CViewModelActivityTasks"
    //Список дисциплин, отображаемых на главной активности.
    val tasks                               = MutableLiveData<List<CTask>>()
    val deadline                            : LocalDateTime?
                                            = null
    val allStatuses                         = MutableLiveData<List<CStatus>>()
    val objects                             = MutableLiveData<List<CObject>>()
    //Признак того, что происходит актуализация данных.
    //Влияет на видимость индикатора
    val loading                             = MutableLiveData<Boolean>()
    val taskId                              : UUID?
                                            = null


    private var objectId                    :UUID?
                                            = null

    private var _workspaceId                : UUID?
                                            = null
    var workspaceId                         : UUID
        get() = _workspaceId!!
        set(value) {
            _workspaceId                    = value
            loading.value                   = true

            val flowableObject              = repositoryObject.getListFromServer(workspaceId)
                .filter { resource ->
                    resource.isSuccess
                }
//                .doOnNext { resource ->
//                    objects.postValue(resource.data)
//                }

            val flowableUser                = repositoryUser.saveListFromServerToDB(workspaceId)
                .filter { resource ->
                    resource.isSuccess
                }
//                .doOnNext { resource ->
//                    objects.postValue(resource.data)
//                }

            val flowableTasks               = Flowables.zip(flowableObject, flowableUser)
                .flatMap {
                    repositoryTask.getListFromServer(workspaceId)
                }
                .filter { resource ->
                    resource.isSuccess
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                        resource -> tasks.postValue(resource.data)
                }

            registerDisposable(flowableTasks.subscribe ())
        }

    init {

        registerDisposable(repositoryStatus.getListFromServer()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {

                    resource -> allStatuses.postValue(resource.data)
            }
        )
    }

    fun getStatusName(task : CTask?) : String
    {
        task ?: return ""

        val test = task.statusId
        return if (test == null)
            ""
        else {
            val statusName                  = allStatuses.value!!
                .asSequence()
                .filter { it.id == task.statusId }
                .map { it.name }
                .firstOrNull()


            statusName ?: return ""

            val resId                       = context.resources.getIdentifier(statusName, "string", context.packageName)
            return context.getString(resId)

            }
    }

    fun getStatusColor(task : CTask) : Int
    {
        val test = task.statusId
        return if (test == null)
            R.color.white
        else {
            val statusName                  = allStatuses.value!!
                .asSequence()
                .filter { it.id == task.statusId }
                .map { it.name }
                .firstOrNull()


            statusName ?: return R.color.white
            return ContextCompat.getColor(context, context.resources.getIdentifier(statusName, "color", context.packageName))

//            return when (statusName)
//            {
//                "StatusNameTicketNew" ->
//                else -> "#6f7180"
//            }

        }
    }

    fun getStringDeadline (task : CTask) : String
    {
        val a = task.deadline
        if (a != null)
            return CConverterLocalDateTime.dateToString(a)
        return ""
    }

}
