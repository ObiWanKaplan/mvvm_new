package ru.psu.mobileapp.viewmodel

interface IClickListener {
    fun onItemClick(item: Any)
}
