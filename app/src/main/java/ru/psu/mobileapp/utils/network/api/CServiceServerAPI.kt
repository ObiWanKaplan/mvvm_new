package ru.psu.mobileapp.utils.network.api

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import net.openid.appauth.AuthState
import ru.psu.mobileapp.dto.CDTOObject
import ru.psu.mobileapp.model.CStatus
import ru.psu.mobileapp.model.CSubject
import ru.psu.mobileapp.model.CWorkSpace
import ru.psu.mobileapp.dto.CDTOTask
import ru.psu.mobileapp.dto.CDTOUser
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/********************************************************************************************************
 * Сервис отвечает за взаимодействие с API сервера данных.                                              *
 * @author Селетков И.П. 2018 0817.                                                                     *
 *******************************************************************************************************/
@Singleton
class CServiceServerAPI
@Inject constructor
(
    private val template                    : IServerAPITemplate,
    private  val authState                  : AuthState
)                                           : IServiceServerAPI
{
    /****************************************************************************************************
     * Запрос в API сервера списка дисциплин, которые должен проходить студент с учётной записью        *
     * [userId].                                                                                        *
     * @param userId - идентификатор учётной записи пользователя, для которой необходимо получить       *
     * список дисциплин.                                                                                *
     * @return объект для отслеживания статуса запроса.                                                 *
     ***************************************************************************************************/
    override fun fetchSubjects(
        userId                              : UUID
    )                                       : Single<List<CSubject>>
    {
        return template.getSubjectsByUserId(userId.toString())
            //Выполняем операцию в фоновом потоке, чтобы не блокировать интерфейс.
            .subscribeOn(Schedulers.io())
    }
    /****************************************************************************************************
     * Запрос в API сервера списка рабочих областей,                                                     *
     * список дисциплин.                                                                                *
     * @return объект для отслеживания статуса запроса.                                                 *
     ***************************************************************************************************/
    override fun fetchWorkSpaces(
    )                                       : Single<List<CWorkSpace>>
    {
        return template.getWorkSpaces("Bearer ${authState.accessToken}")
            .subscribeOn(Schedulers.io())
    }

    override fun fetchTasks(
        workspaceId                         : UUID
    )                                       : Single<List<CDTOTask>>
    {
        return template.getTasksByWorkspaceId("Bearer ${authState.accessToken}", workspaceId.toString())
            .subscribeOn(Schedulers.io())
    }

    override fun fetchStatuses(
    )                                       : Single<List<CStatus>>
    {
        return template.getStatus("Bearer ${authState.accessToken}")
            .subscribeOn(Schedulers.io())
    }

    override fun fetchAvailableStatuses(
        statusId                            : UUID
    )                                       : Single<List<CStatus>>
    {
        return template.getAvailableStatuses("Bearer ${authState.accessToken}", statusId.toString())
            .subscribeOn(Schedulers.io())
    }

    override fun fetchObjects(
        workSpaceId                         : UUID
    )                                       : Single<List<CDTOObject>>
    {
        return template.getObjects(
            "Bearer ${authState.accessToken}",
            workSpaceId.toString()
            )
            .subscribeOn(Schedulers.io())
    }

    override fun fetchUsers(
        workSpaceId                         : UUID
    )                                       : Single<List<CDTOUser>>
    {
        return template.getUsers("Bearer ${authState.accessToken}",
        workSpaceId.toString())
            .subscribeOn(Schedulers.io())
    }


}