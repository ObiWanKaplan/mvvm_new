package ru.psu.mobileapp.utils.network.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query
import ru.psu.mobileapp.dto.CDTOObject
import ru.psu.mobileapp.model.CStatus
import ru.psu.mobileapp.model.CSubject
import ru.psu.mobileapp.model.CWorkSpace
import ru.psu.mobileapp.dto.CDTOTask
import ru.psu.mobileapp.dto.CDTOUser
import ru.psu.mobileapp.model.CObject
import java.util.*

/********************************************************************************************************
 * Интерфейс описывает запросы к API сервера.                                                           *
 * @author Селетков И.П. 2019 0211.                                                                     *
 *******************************************************************************************************/
interface IServerAPITemplate
{
    /****************************************************************************************************
     * Запрос списка дисциплин, которые изучает студент с учётной записью с идентификатором [userId].   *
     * по данным АСУ ТП.                                                                                *
     * @param userId - идентификатор учётной записи пользователя.                                       *
     * @return объект с возможность отслеживания статуса запроса.                                       *
     ***************************************************************************************************/
    @GET("subjects")
    fun getSubjectsByUserId(
        @Query("user_id")
        userId                              : String
    )                                       : Single<List<CSubject>>
    /****************************************************************************************************
     * Запрос списка рабочих областей, доступных пользователю.                                          *
     * по данным АСУ ТП.                                                                                *
     * @return объект с возможность отслеживания статуса запроса.                                       *
     ***************************************************************************************************/
    @GET("project/api/workspaces")
    fun getWorkSpaces(
        @Header("Authorization") token:String
    )        : Single<List<CWorkSpace>>


    /****************************************************************************************************
     * Запрос списка рабочих областей, c идентификатором [workspace_id].                                *
     * по данным АСУ ТП.                                                                                *
     * @param workspaceId   - идентификатор учётной записи пользователя.                               *
     * @return объект с возможность отслеживания статуса запроса.                                       *
     ***************************************************************************************************/
    @GET("project/api/tasks")
    fun getTasksByWorkspaceId(
        @Header("Authorization")
        token                               : String,
        @Query("workspace_id")
        workspaceId                         : String
    )                                       : Single<List<CDTOTask>>

    /****************************************************************************************************
     * Запрос списка статусов, доступных пользователю.                                                  *
     * по данным АСУ ТП.                                                                                *
     * @return объект с возможность отслеживания статуса запроса.                                       *
     ***************************************************************************************************/
    @GET("project/api/statuses")
    fun getStatus(
        @Header("Authorization") token:String
    )                                        : Single<List<CStatus>>

    /****************************************************************************************************
     * Запрос списка статусов, на которые возможно перейти из статуса, c идентификатором [statusId].   *
     * по данным АСУ ТП.                                                                                *
     * @param statusId   - идентификатор учётной записи пользователя.                                  *
     * @return объект с возможность отслеживания статуса запроса.                                       *
     ***************************************************************************************************/
    @GET("project/api/statuses")
    fun getAvailableStatuses(
        @Header("Authorization")
        token                               : String,
        @Query("status_id")
        statusId                            : String

    )                                       : Single<List<CStatus>>

    /****************************************************************************************************
     * Запрос списка рабочих областей, доступных пользователю.                                          *
     * по данным АСУ ТП.                                                                                *
     * @return объект с возможность отслеживания статуса запроса.                                       *
     ***************************************************************************************************/
    @GET("project/api/objects")
    fun getObjects(
        @Header("Authorization") token: String,
        @Query("workspace_id")
        workSpaceId                         : String

    )                                       : Single<List<CDTOObject>>

    /****************************************************************************************************
     * Запрос списка рабочих областей, доступных пользователю.                                          *
     * по данным АСУ ТП.                                                                                *
     * @return объект с возможность отслеживания статуса запроса.                                       *
     ***************************************************************************************************/
    @GET("project/api/users")
    fun getUsers(
        @Header("Authorization") token: String,
        @Query("workspace_id")
        workSpaceId                         : String
    )                                       : Single<List<CDTOUser>>


}