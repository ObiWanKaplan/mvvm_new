package ru.psu.mobileapp

import android.content.Context
import androidx.multidex.MultiDex
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import ru.psu.mobileapp.di.components.DaggerIComponentApp
import ru.psu.mobileapp.di.components.IComponentApp

/********************************************************************************************************
 * Основной класс приложения (точка входа выполнения).                                                  *
 * @author Селетков И.П. 2019 0212.                                                                     *
 *******************************************************************************************************/
class CApplication                          : DaggerApplication() {
    companion object {
        @JvmStatic
        lateinit var componentApp: IComponentApp
            private set
    }

    override fun applicationInjector(): AndroidInjector<out CApplication> {
        initAppComponent()
        return componentApp
    }

    /****************************************************************************************************
     * Настройка компонента для внедрения в других местах.                                              *
     * https://android.jlelse.eu/new-android-injector-with-dagger-2-part-3-fe3924df6a89                 *
     ***************************************************************************************************/
    private fun initAppComponent() {
        componentApp = DaggerIComponentApp
            .builder()
            .create(this) as IComponentApp
    }

    override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)

        initOkta()
    }
    private fun initOkta()
    {

    }
}